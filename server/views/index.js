import React from 'react';
import PropTypes from 'prop-types';

import Layout from './layout';

const defaultProps = {};

const propTypes = {
	title: PropTypes.string.isRequired
};

const Index = ({
	title
}) => (
	<Layout title={title}>
		<div id="app" />
	</Layout>
);

Index.defaultProps = defaultProps;
Index.propTypes = propTypes;

export default Index;
