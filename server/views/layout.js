import React from 'react';
import PropTypes from 'prop-types';

const defaultProps = {};

const propTypes = {
	title: PropTypes.string.isRequired,
	children: PropTypes.element.isRequired,
};

const Layout = ({
	title,
	children,
}) => (
	<html>
		<head>
			<title>
				{title}
			</title>
			<link rel="stylesheet" href="./dist/main.css" />
		</head>
		<body className="flex flex-col justify-center items-center min-h-screen">
			{children}
			<script src="./dist/main-bundle.js" />
		</body>
	</html>
);

Layout.defaultProps = defaultProps;
Layout.propTypes = propTypes;

export default Layout;
