import React from 'react';

const Spin = () => (
  <div className="autocomplete--spin-wrapper">
    <div className="autocomplete--spin" />
  </div>
);

export default Spin;
