import React from 'react';
import PropTypes from 'prop-types';

import phrases from './phrases';

const ChevronIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    className="autocomplete-icon"
    fill="currentColor"
  >
    <path fillRule="evenodd" d="M3.646 9.146a.5.5 0 01.708 0L8 12.793l3.646-3.647a.5.5 0 01.708.708l-4 4a.5.5 0 01-.708 0l-4-4a.5.5 0 010-.708zm0-2.292a.5.5 0 00.708 0L8 3.207l3.646 3.647a.5.5 0 00.708-.708l-4-4a.5.5 0 00-.708 0l-4 4a.5.5 0 000 .708z" />
  </svg>
);

const defaultProps = {
  ariaLabel: phrases.open,
  children: <ChevronIcon />,
  onClick: () => {},
};

const propTypes = {
  ariaLabel: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

const Open = ({
  ariaLabel,
  children,
  onClick,
}) => (
  <button
    className="autocomplete--search-open"
    aria-label={ariaLabel}
    onClick={onClick}
    type="button"
  >
    {children}
  </button>
);

Open.defaultProps = defaultProps;
Open.propTypes = propTypes;

export default Open;
