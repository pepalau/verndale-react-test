const getOptionFor = ({
  options,
  selected,
  getValueForOption,
}) => options.find((o) => getValueForOption(o) === selected);

export default getOptionFor;
