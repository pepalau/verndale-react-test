import React from 'react';
import PropTypes from 'prop-types';

const defaultProps = {
  isOpen: false,
  ariaLabel: 'Listbox',
  children: null,
};

const propTypes = {
  htmlId: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  ariaLabel: PropTypes.string,
  children: PropTypes.node,
};

const Suggestions = ({
  htmlId,
  isOpen,
  ariaLabel,
  children,
}) => (
  <ul
    id={htmlId}
    className="autocomplete--items"
    aria-expanded={isOpen}
    aria-label={ariaLabel}
    role="listbox"
  >
    {children}
  </ul>
);

Suggestions.defaultProps = defaultProps;
Suggestions.propTypes = propTypes;

export default Suggestions;
