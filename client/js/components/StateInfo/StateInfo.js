import React from 'react';
import PropTypes from 'prop-types';

const defaultProps = {
  state: null,
};

const propTypes = {
  state: PropTypes.shape({
    name: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
  }),
};

const StateInfo = ({
  state,
}) => {
  if (!state) {
    return null;
  }

  const {
    name,
    abbreviation,
  } = state;

  return (
    <div className="font-mono w-full">
      <div className="flex items-center mb-2 w-full">
        <div className="font-bold">
          State
        </div>
        <div className="flex-1 border-t border-dashed mx-1" />
        {name}
      </div>
      <div className="flex items-center mb-2 w-full">
        <div className="font-bold">
          Abbreviation
        </div>
        <div className="flex-1 border-t border-dashed mx-1" />
        {abbreviation}
      </div>
    </div>
  );
};

StateInfo.defaultProps = defaultProps;
StateInfo.propTypes = propTypes;

export default StateInfo;
