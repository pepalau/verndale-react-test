# Verndale JavaScript/React code test

Create an autocomplete component that allows users to enter a search term to search and select a state from the U.S.

__Important__: This is a test of functionality, you do not need to create a pixel perfect design.

The purpose of this test is to examine your ability to write code and structure a small React component.

A reset css file is already imported in to the project as well as some stubbed out files, so you are ready to start writing your code.

`client/js` and `client/sass` are the directories where you will be working in.

## Functional Specs

See `component-wireframe.pdf` for wireframes.

- Start searching after 2 or more characters are entered in to the input field
- Navigate up and down through the results with the arrow keys - as you're navigating, the input value will update with the appropriate label
- Select a result with either hitting "return" or a mouse click when over a result
- Upon selection of a result, result list should go away
- Show a max of 5 items in the result before vertical overflow is visible

## Required build tools

- [Node.js](https://nodejs.org/en/)
- [Webpack](https://webpack.js.org/)

## Required tools/libs/frameworks:

- [React](https://reactjs.org/)
- [SASS](https://sass-lang.com/)

## Instructions

- Open up terminal and navigate to the root of the project
- Run: `npm install`
- After installation is complete, run: `webpack`
- After compilation, open your browser and navigate to `localhost:3000`

When you make a change to a Javascript or SCSS file in the `client` folder it will automatically compile, you will have to refresh your browser manually to see the change.

## API

- localhost:3000/api/states?term=`[search term]`
